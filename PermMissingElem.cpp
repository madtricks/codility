#include <numeric>
#include <functional>


//warning: 
//  third and fourth arguments to std accumulate must be 0L and std::plus<long> 
//  respectively otherwise sum will overflow (and signed integer overflow causes
//  the program to be ill-formed as per the c++ standard (although it will possibly 
//  continue to work). Use of _unsigned_ int can be considered as an alternative.

int solution(vector<int> &A) {
    const size_t N = A.size();
    long sum = std::accumulate(A.begin(), A.end(), 0L, std::plus<long>());
    long expected_sum = ((N+2)*(N+1)) >> 1;
    return expected_sum - sum;
}
