#include <algorithm>

int solution(vector<int> &A) {
    const int N = A.size();
    
    using disk_t = std::pair<long, bool>;
    
    vector<disk_t> disks;
    
    for (long i = 0; i < N; ++i) {
        disks.push_back(std::make_pair(i-A[i],false));
        disks.push_back(std::make_pair(i+A[i],true));
    }
    std::sort(disks.begin(), disks.end());
    
    int open_disks = 0;
    int disk_intersected = 0;
    
    for (const auto& disk : disks) {
        if (!disk.second) {
            disk_intersected += open_disks;   
            if (disk_intersected > 1e7) 
                return -1;
            open_disks++;
        }
        else {
            open_disks--;
        }
    }
    return disk_intersected; 
}