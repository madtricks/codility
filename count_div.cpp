int solution(int A, int B, int K) {
    int ksUpToA = (A-1)/K;
    int ksUpToB = B/K;
    return (A==0)? ksUpToB + 1 : ksUpToB - ksUpToA; 
}