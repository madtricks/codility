int solution(vector<int> &A) {
    const auto N = A.size();
    auto n = N;
    
    vector<bool> contains(N);
    
    for (const auto& idx : A) {
        auto _idx = idx -1;  
        if (_idx < 0 || _idx >= N || contains[_idx]) return 0;
        contains[_idx] = true;
        --n;
    }
    
    return n == 0;
    
}
