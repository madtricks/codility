vector<int> solution(int N, vector<int> &A) {
    vector<int> counters(N);
    
    auto minimal_cv = 0;
    auto running_max_cv = 0;
    
    for (const auto& op : A) {
        if (op == N+1) 
            minimal_cv = running_max_cv;
        else {
            if (counters[op-1] < minimal_cv)
                counters[op-1] = minimal_cv;
            running_max_cv = max(++counters[op-1],running_max_cv);
        }
    }
    
    for (auto& counter : counters) {
        counter = max(counter,minimal_cv); 
    }
    
    return counters;
    
}
