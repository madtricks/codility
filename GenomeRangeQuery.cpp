// you can use includes, for example:
// #include <algorithm>
#include <cassert>
// you can write to stdout for debugging purposes, e.g.
// cout << "this is a debug message" << endl;

constexpr size_t FUNC_MAP_MAX = 4u;

template <typename I, typename F>
vector<vector<int>> get_prefix_counts(I first, I last, F map_func) {
    vector<vector<int>> res(FUNC_MAP_MAX);
    
    for(auto& v : res) {
        v.reserve(last-first);
        v.push_back(0);
    }
    
    while (first != last) {
        for (size_t i = 0; i < FUNC_MAP_MAX; ++i) {
            res[i].push_back(res[i].back());
        }
        res[map_func(*first)-1].back() += 1;
        ++first;
    }
    return res;
}


vector<int> solution(string &S, vector<int> &P, vector<int> &Q) {
    const size_t M = P.size();
    
    auto map_func = [](string::value_type c) -> int {
        switch(c) {
            case 'A': return 1;
            case 'C': return 2;
            case 'G': return 3;
            case 'T': return 4;
            default: assert(false);
        }
    };
    auto prefix_counts = get_prefix_counts(S.begin(),S.end(), map_func);
    vector<int> res(M);
    
    for (size_t q = 0; q < M; ++q) {
        int val = FUNC_MAP_MAX;
        for (size_t i = FUNC_MAP_MAX; i -->0;) {
            int pot_val = prefix_counts[i][Q[q]+1] - prefix_counts[i][P[q]];
            if (pot_val) 
                val = i+1;
        }
        res[q] = val;
    }
    
    return res;
}