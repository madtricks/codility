int solution(int X, int Y, int D) {
    auto d = (Y-X);
    auto q = d/D;
    return (q*D >= d)? q : q+1;
}
