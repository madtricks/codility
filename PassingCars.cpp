int solution(vector<int> &A) {
    const auto N = A.size();
    const auto max_passing_v = 1000000000u;
    
    auto eastward_cars = 0u;
    auto passing_cars = 0u;
    
    for (auto i = 0u; i < N; ++i) {
        if (A[i]) {
            passing_cars += eastward_cars;
            if (passing_cars > max_passing_v)
                return -1;
        }
        else 
            ++eastward_cars;
    }
    return passing_cars;
    
}
