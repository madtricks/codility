int solution(vector<int> &A) {
    const size_t N = A.size();
    
    double avg = (A[0]+A[1])/2.0;
    size_t idx = 0;
    for (size_t i = 0; i < N-2; ++i) {
        int sum = A[i] + A[i+1];
        
        if (sum/2.0 < avg) {
            avg = sum/2.0;
            idx = i;
        }
        
        sum += A[i+2];
        
        if (sum/3.0 < avg) {
            avg = sum/3.0;
            idx = i;
        }
    }
    if ((A[N-2] + A[N-1]) /2.0 < avg)
        idx = N-2;
    return idx;
 }