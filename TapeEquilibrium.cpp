#include <algorithm>
#include <numeric>

int solution(vector<int> &A) {
    auto right_sum = std::accumulate(A.begin() + 1, A.end(),0);
    auto left_sum = A[0];

    auto min = std::abs(left_sum - right_sum);

    for (size_t i = 1; i < A.size()-1; ++i) {
        left_sum += A[i];
        right_sum -= A[i];
        min = std::min(min, std::abs(left_sum - right_sum));
    }

    return min;
}
