int solution(int X, vector<int> &A) {
    const int N = A.size();
    vector<bool> passable(X);
    
    for (size_t i = 0; i < N; ++i) {
        if (!passable[A[i]-1]) {
             passable[A[i]-1] = true;
             if (--X == 0) return i;
        }
    }
    return -1;   
}
