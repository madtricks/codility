#include <algorithm>

int solution(vector<int> &A) {
    const auto N= A.size();
    vector<bool> contains(N);
    
    for (const auto& idx : A) {
        size_t _idx = idx-1;
        if (_idx < N) 
            contains[_idx] = true;
    }
    auto it = find(begin(contains),end(contains),0);
    return std::distance(begin(contains),it)+1;    
}
